import React, { useEffect, useCallback } from 'react'
import { useSelector, useDispatch } from 'react-redux'

import Container from './components/Container'
import Header from './components/Header'
import Tabs from './components/Tabs'
import Tab from './components/Tab'
import Loader from './components/Loader'
import Transactions from './components/Transactions'

const App = () => {
  const dispatch = useDispatch()
  const realBills = useSelector((state) => state.realBills)
  const potentialBills = useSelector((state) => state.potentialBills)
  const loading = useSelector((state) => state.loading)
  const updateBill = useCallback((id, value) =>
    dispatch({ type: 'UPDATE_BILL', id, value })
  )

  useEffect(() => {
    dispatch({ type: 'GET_BILLS' })
  }, [])

  return (
    <>
      <Header />
      <Container>
        {loading && <Loader dataTestId="loader" />}
        <Tabs>
          <Tab title="Bills">
            <Transactions
              dataTestId="real_bills_list"
              transactions={realBills}
              updateBill={updateBill}
            />
          </Tab>
          <Tab title="Potential Bills">
            <Transactions
              dataTestId="potential_bills_list"
              transactions={potentialBills}
              updateBill={updateBill}
            />
          </Tab>
        </Tabs>
      </Container>
    </>
  )
}

export default App
