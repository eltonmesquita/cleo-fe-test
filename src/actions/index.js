export const getBills = () => ({
  type: 'GET_BILLS',
})

export const updateBill = (id, value) => ({
  type: 'UPDATE_BILL',
  id,
  value,
})

export const updateTab = (tabs) => ({
  type: 'UPDATE_TABS',
  tabs,
})

export const updateActiveTab = (id) => ({
  type: 'UPDATE_ACTIVE_TAB',
  id,
})
