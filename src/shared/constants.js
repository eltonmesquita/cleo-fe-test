export const StyleConstants = {
  colors: {
    brand: '#0815FF',
    highlight: '#36e301',
    background: '#f6f7fa',
    borders: '#d8d8d8',
    caution: '#ff5e00',
  },
}

export const APIConstants = {
  base: 'http://localhost:3002/',
}
