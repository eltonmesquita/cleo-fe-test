export const data = [
  {
    categoryId: 1,
    iconUrl:
      'https://pbs.twimg.com/profile_images/915824582455627776/nsyHCFq9.jpg',
    id: 'vodafone',
    isBill: true,
    name: 'Vodafone',
    transactions: [
      {
        amount: 12.34,
        date: '2018-01-13',
        id: 36,
      },
      {
        amount: 14.34,
        date: '2018-02-13',
        id: 37,
      },
      {
        amount: 15.54,
        date: '2018-03-13',
        id: 38,
      },
      {
        amount: 11.34,
        date: '2018-04-13',
        id: 39,
      },
      {
        amount: 18.99,
        date: '2018-05-13',
        id: 40,
      },
    ],
  },
  {
    categoryId: 2,
    iconUrl:
      'https://pbs.twimg.com/profile_images/787957563463725056/duc0g4fp.jpg',
    id: 'sky_tv',
    isBill: false,
    name: 'Sky TV',
    transactions: [
      {
        amount: 82.17,
        date: '2018-01-01',
        id: 41,
      },
      {
        amount: 82.17,
        date: '2018-02-01',
        id: 42,
      },
      {
        amount: 82.17,
        date: '2018-03-01',
        id: 43,
      },
      {
        amount: 82.17,
        date: '2018-04-01',
        id: 44,
      },
      {
        amount: 82.17,
        date: '2018-05-01',
        id: 45,
      },
    ],
  },
]
