import React from 'react'
import { render } from '@testing-library/react'
import createSagaMiddleware from 'redux-saga'
import { createStore, compose, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'

import reducer from '../reducers'
import rootSaga from '../sagas'

const sagaMiddleware = createSagaMiddleware()

const store = createStore(reducer, compose(applyMiddleware(sagaMiddleware)))

sagaMiddleware.run(rootSaga)

const AllTheProviders = ({ children }) => {
  return <Provider store={store}>{children}</Provider>
}

const customRender = (ui, options) =>
  render(ui, { wrapper: AllTheProviders, ...options })

// re-export everything
export * from '@testing-library/react'

// override render method
export { customRender as render }
