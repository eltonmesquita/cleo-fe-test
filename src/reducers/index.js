import { partition } from 'lodash/fp'

const intialState = {
  loading: true,
  realBills: [],
  potentialBills: [],
  activeTab: '',
  tabItems: [],
}

const updateBillsLocally = (bills, bill) =>
  bills.map((item) => (item.id === bill.id ? bill : item))

const splitBills = (bills) => partition((b) => b.isBill && b)(bills)

const reducer = (state = intialState, action) => {
  switch (action.type) {
    case 'GET_BILLS':
      return { ...state, loading: true }
    case 'BILLS_RECEIVED':
      const [realBills, potentialBills] = splitBills(action.bills)
      return {
        ...state,
        bills: action.bills,
        realBills,
        potentialBills,
        loading: false,
      }
    case 'BILLS_REQUEST_FAILED':
      return { ...state, error: action.error, loading: false }
    case 'UPDATE_BILL':
      return { ...state, loading: true }
    case 'BILL_UPDATED':
      const updatedBills = updateBillsLocally(state.bills, action.bill)
      const [updatedRealBills, updatedPotentialBills] = splitBills(updatedBills)

      return {
        ...state,
        bills: updatedBills,
        realBills: updatedRealBills,
        potentialBills: updatedPotentialBills,
        loading: false,
      }
    case 'BILL_UPDATE_FAILED':
      return { ...state, error: action.error, loading: false }
    case 'UPDATE_TABS':
      return { ...state, tabItems: action.tabs }
    case 'UPDATE_ACTIVE_TAB':
      return { ...state, activeTab: action.id }
    default:
      return state
  }
}

export default reducer
