import { put, takeLatest, all } from 'redux-saga/effects'

import { APIConstants } from '../shared/constants'

function* getBills() {
  try {
    const bills = yield window
      .fetch(`${APIConstants.base}bills`)
      .then((res) => res.json())
      .then((res) => res)

    yield put({ type: 'BILLS_RECEIVED', bills })
  } catch (error) {
    yield put({ type: 'BILLS_REQUEST_FAILED', error })
  }
}

function* updateBill({ id, value }) {
  try {
    const bill = yield window
      .fetch(`${APIConstants.base}bills/${id}`, {
        headers: {
          'Content-Type': 'application/json',
        },
        method: 'PATCH',
        body: JSON.stringify(value),
      })
      .then((res) => res.json())
      .then((res) => res)

    yield put({ type: 'BILL_UPDATED', bill })
  } catch (error) {
    yield put({ type: 'BILL_UPDATE_FAILED', error })
  }
}

function* getBillsWatcher() {
  yield takeLatest('GET_BILLS', getBills)
}

function* updateBillWatcher() {
  yield takeLatest('UPDATE_BILL', updateBill)
}

function* rootSaga() {
  yield all([getBillsWatcher(), updateBillWatcher()])
}

export default rootSaga
