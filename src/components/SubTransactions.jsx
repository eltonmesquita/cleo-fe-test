import React from 'react'
import styled from 'styled-components'

import { StyleConstants } from '../shared/constants'

const List = styled.ul`
  margin: 0;
  padding: 0;

  list-style: none;
`

const Item = styled.li`
  padding: 1rem;

  border-bottom: 1px solid ${StyleConstants.colors.borders};

  font-size: 0.75rem;
`

var formatter = new Intl.NumberFormat('en', {
  style: 'currency',
  currency: 'GBP',
})

const SubTransactions = ({ transactions, dataTestId }) => (
  <List data-testid={dataTestId}>
    {transactions.map(({ amount, date, id }) => (
      <Item key={id}>
        {date} - {formatter.format(amount)}
      </Item>
    ))}
  </List>
)

export default SubTransactions
