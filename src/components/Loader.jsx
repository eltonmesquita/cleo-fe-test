import React from 'react'
import styled from 'styled-components'

import loader from '../assets/loader.gif'

const Container = styled.aside`
  position: absolute;
  top: 1rem;
  right: 1rem;
  z-index: 100;
`

const Loader = ({ size = 60, dataTestId }) => (
  <Container data-testid={dataTestId}>
    <picture>
      <img src={loader} alt="" width={size} />
    </picture>
  </Container>
)

export default Loader
