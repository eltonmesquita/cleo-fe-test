import React from 'react'

const Tab = ({ title, children }) => <section>{children}</section>

export default Tab
