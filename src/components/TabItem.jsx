import React from 'react'
import styled from 'styled-components'

const Body = styled.article``

const TabItem = ({ items, activeTab }) => (
  <article>
    {items.map(
      ({ c, title, id }) => id === activeTab && <Body key={id}>{c}</Body>
    )}
  </article>
)

export default TabItem
