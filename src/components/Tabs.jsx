import React, { Children, useEffect, useCallback } from 'react'
import { useSelector, useDispatch } from 'react-redux'

import TabNav from './TabNav'
import TabItem from './TabItem'

const Tabs = ({ children }) => {
  const dispatch = useDispatch()

  const activeTab = useSelector((state) => state.activeTab)
  const items = useSelector((state) => state.tabItems) || []

  const onClickTab = useCallback(
    (id) => dispatch({ type: 'UPDATE_ACTIVE_TAB', id }),
    [dispatch]
  )
  const createId = (title, index) => `${title}_${index}`

  useEffect(() => {
    const tabs = Children.map(children, (c, i) => {
      const { title } = c.props
      const id = createId(title, i)

      if (i === 0 && !activeTab) dispatch({ type: 'UPDATE_ACTIVE_TAB', id })

      return { c, title, id }
    })

    dispatch({ type: 'UPDATE_TABS', tabs })
  }, [children])

  return (
    <section>
      <TabNav onClick={onClickTab} items={items} activeTab={activeTab} />
      <TabItem items={items} activeTab={activeTab} />
    </section>
  )
}

export default Tabs
