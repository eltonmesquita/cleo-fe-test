import React, { useState } from 'react'
import styled from 'styled-components'

import Button from './Button'
import { Arrow } from '../icons'
import { StyleConstants } from '../shared/constants'
import SubTransactions from './SubTransactions'

const List = styled.ul`
  margin: 0;
  padding: 0;

  list-style: none;
`

const Title = styled.h3`
  position: relative;
  vertical-align: middle;

  cursor: pointer;
`

const Image = styled.img`
  display: inline-block;
  vertical-align: middle;
  overflow: hidden;

  width: 40px;
  height: 40px;

  border-radius: 100%;
  background: ${StyleConstants.colors.background};
`

const Item = styled.li`
  padding: 1rem;
  border-bottom: 1px solid ${StyleConstants.colors.borders};
`

const ArrowWrap = styled.div`
  position: absolute;
  top: calc(50% - 9px);
  right: 1rem;
`

const Counter = styled.div`
  position: absolute;
  bottom: -10px;
  left: 22px;

  display: flex;
  justify-content: center;
  align-items: center;

  width: 22px;
  height: 22px;

  color: ${StyleConstants.colors.brand};
  background: ${StyleConstants.colors.highlight};
  border-radius: 100%;

  text-align: center;
  font-size: 0.85rem;
`

const CounterInner = styled.span`
  line-height: 1;
`
const ButtonWrap = styled.div`
  text-align: right;
`

const Transactions = ({ transactions, updateBill, dataTestId }) => {
  const [activeTransaction, setActiveTransaction] = useState('')

  const onClickBill = (id) => {
    if (activeTransaction !== id) {
      setActiveTransaction(() => id)
    } else {
      setActiveTransaction(() => '')
    }
  }

  return (
    <List data-testid={dataTestId}>
      {transactions.map((bill) => (
        <Item data-testid={`bill_${bill.name}`} key={bill.id}>
          <Title onClick={() => onClickBill(bill.id)}>
            <Image src={bill.iconUrl} width={40} height={40} /> {bill.name}
            <Counter>
              <CounterInner>{bill.transactions.length}</CounterInner>
            </Counter>
            <ArrowWrap>
              <Arrow
                direction={activeTransaction === bill.id ? 'down' : 'right'}
              />
            </ArrowWrap>
          </Title>

          {activeTransaction === bill.id && (
            <>
              <ButtonWrap>
                <Button
                  caution={!bill.isBill}
                  onClick={() => updateBill(bill.id, { isBill: !bill.isBill })}
                >
                  This is {bill.isBill ? 'not ' : ' '}a Bill
                </Button>
              </ButtonWrap>
              <SubTransactions
                dataTestId={`transactions_${bill.name}`}
                transactions={bill.transactions}
              />
            </>
          )}
        </Item>
      ))}
    </List>
  )
}

export default Transactions
