import styled from 'styled-components'

const Container = styled.div`
  position: relative;
  width: 500px;
  max-width: 100%;

  margin: 0 auto;

  background: #fff;
  box-shadow: rgba(0, 0, 0, 0.1) 1px 1px 10px;
`

export default Container
