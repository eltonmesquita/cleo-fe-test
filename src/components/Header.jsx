import React from 'react'
import styled from 'styled-components'
import { StyleConstants } from '../shared/constants'

import { Logo } from '../icons'

const Container = styled.header`
  padding: 1rem;
  margin-bottom: 2rem;

  background: ${StyleConstants.colors.brand};

  text-align: center;
`

const Header = () => (
  <Container>
    <Logo />
  </Container>
)

export default Header
