import styled from 'styled-components'
import { StyleConstants } from '../shared/constants'

const Button = styled.button`
  border: 0;
  padding: 0.4em 1em;

  cursor: pointer;
  color: #fff;
  background: ${(props) =>
    props.caution
      ? StyleConstants.colors.brand
      : StyleConstants.colors.caution};
`

export default Button
