import React from 'react'
import styled from 'styled-components'

import { StyleConstants } from '../shared/constants'

const List = styled.ul`
  display: flex;
  flex-wrap: nowrap;
  justify-content: space-evenly;

  padding: 0;
  margin: 0;

  list-style: none;
`

const Item = styled.li`
  flex-grow: 1;

  padding: 2rem 1rem;

  cursor: pointer;
  background: ${(props) =>
    props.active ? '#fff' : StyleConstants.colors.background};
  border-color: ${(props) =>
    props.active ? StyleConstants.colors.brand : 'transparent'};
  border-bottom-style: solid;
  border-bottom-width: ${(props) => (props.active ? '3px' : '0')};

  text-align: center;
`

const TabNav = ({ items, onClick, activeTab }) => (
  <nav>
    <List>
      {items.map(({ c, title, id }) => (
        <Item active={activeTab === id} key={id} onClick={() => onClick(id)}>
          {title}
        </Item>
      ))}
    </List>
  </nav>
)

export default TabNav
