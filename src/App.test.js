import React from 'react'
import fetchMock from 'fetch-mock-jest'
import App from './App'
import { APIConstants } from './shared/constants'

import {
  render,
  fireEvent,
  cleanup,
  waitForElementToBeRemoved,
} from './utils/test.utils.js'
import { data } from './utils/mockData'

beforeEach(cleanup)

describe('when the application runs', () => {
  fetchMock
    .get(`${APIConstants.base}bills`, data)
    .patch(`${APIConstants.base}bills/vodafone`, {
      ...data[0],
      isBill: false,
    })
    .patch(`${APIConstants.base}bills/sky_tv`, {
      ...data[1],
      isBill: true,
    })

  it('renders without crashing', () => {
    const wrapper = render(<App />)

    wrapper.getByText('Bills')
    wrapper.getByText('Potential Bills')
  })

  it('renders list of Bills', async () => {
    const wrapper = render(<App />)

    await wrapper.findByTestId('real_bills_list')
    await wrapper.findByTestId('bill_Vodafone')
    await wrapper.findByText('Vodafone')
  })

  it('renders list of Potential Bills', async () => {
    const wrapper = render(<App />)

    fireEvent.click(wrapper.getByText('Potential Bills'))
    await wrapper.findByTestId('potential_bills_list')
    await wrapper.findByTestId('bill_Sky TV')
    await wrapper.findByText('Sky TV')
  })
})

describe('when the user clicks on a Bill', () => {
  it("shows list of a Bill's transactions", async () => {
    const wrapper = render(<App />)

    fireEvent.click(wrapper.getByText('Bills'))

    await wrapper.findByTestId('real_bills_list')
    await wrapper.findByTestId('bill_Vodafone')
    await wrapper.findByText('Vodafone')

    fireEvent.click(wrapper.getByText('Vodafone'))
    await wrapper.findByTestId('transactions_Vodafone')
    await wrapper.findByText('2018-02-13 - £14.34', { exact: false })
  })

  it('sets a Bill to Potential Bill', async () => {
    const wrapper = render(<App />)
    const { queryByText } = wrapper
    fireEvent.click(wrapper.getByText('Bills'))

    await wrapper.findByTestId('real_bills_list')
    await wrapper.findByTestId('bill_Vodafone')
    await wrapper.findByText('Vodafone')

    fireEvent.click(wrapper.getByText('Vodafone'))
    await wrapper.findByTestId('transactions_Vodafone')

    fireEvent.click(wrapper.getByText('This is not a Bill', { exact: false }))
    await waitForElementToBeRemoved(() => queryByText('Vodafone'))
  })

  it('sets a Potential Bill to Bill', async () => {
    const wrapper = render(<App />)
    const { queryByText } = wrapper
    fireEvent.click(wrapper.getByText('Potential Bills'))

    await wrapper.findByTestId('potential_bills_list')
    await wrapper.findByTestId('bill_Sky TV')
    await wrapper.findByText('Sky TV')

    fireEvent.click(wrapper.getByText('Sky TV'))
    await wrapper.findByTestId('transactions_Sky TV')

    fireEvent.click(wrapper.getByText('This is a Bill', { exact: false }))
    await waitForElementToBeRemoved(() => queryByText('Sky TV'))
  })
})
